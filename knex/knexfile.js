require("dotenv").config()
module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: process.env.DEV_MYSQL_HOST,
      user: process.env.DEV_MYSQL_USER,
      port: parseInt(process.env.DEV_MYSQL_PORT),
      password: process.env.DEV_MYSQL_PASSWORD,
      database: process.env.DEV_MYSQL_DB,
      charset: 'utf8'
    }
  }, pool: {
    min: 2,
    max: 30
  }
}

