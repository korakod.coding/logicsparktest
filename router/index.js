let router = require('express').Router();
router.use("/Order", require("../api/order/routes"))
router.use("/Category", require("../api/category/routes"))
router.use("/Items", require("../api/items/routes"))
router.use("/Login", require("../api/Login/routes"))
router.use("/User", require("../api/user/routes"))
module.exports = router;