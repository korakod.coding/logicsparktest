-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 19, 2022 at 04:05 AM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logic_spark`
--

-- --------------------------------------------------------

--
-- Table structure for table `lsp_category`
--

CREATE TABLE `lsp_category` (
  `c_id` int(11) NOT NULL,
  `c_code` varchar(100) DEFAULT NULL,
  `c_name` varchar(100) DEFAULT NULL,
  `c_description` text,
  `c_status` int(1) DEFAULT '1',
  `c_created_by` varchar(100) DEFAULT NULL,
  `c_modified_by` varchar(100) DEFAULT NULL,
  `c_modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `c_created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lsp_category`
--

INSERT INTO `lsp_category` (`c_id`, `c_code`, `c_name`, `c_description`, `c_status`, `c_created_by`, `c_modified_by`, `c_modified_time`, `c_created_time`) VALUES
(1, 'A001', 'โทรศัพท์', 'สวัสดีค้าบ', 1, 'LGS0005', 'LGS0005', '2022-01-18 16:48:34', '2022-01-18 16:49:20');

-- --------------------------------------------------------

--
-- Table structure for table `lsp_items`
--

CREATE TABLE `lsp_items` (
  `i_id` int(11) NOT NULL,
  `i_name` varchar(100) NOT NULL,
  `i_description` text NOT NULL,
  `i_code` varchar(10) NOT NULL,
  `i_status` int(1) NOT NULL DEFAULT '1',
  `i_created_by` varchar(100) NOT NULL,
  `i_modified_by` varchar(100) NOT NULL,
  `i_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `i_modified_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `i_price_vat` float DEFAULT NULL,
  `i_price` float DEFAULT NULL,
  `i_category` varchar(100) DEFAULT NULL,
  `i_group` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lsp_items`
--

INSERT INTO `lsp_items` (`i_id`, `i_name`, `i_description`, `i_code`, `i_status`, `i_created_by`, `i_modified_by`, `i_created_time`, `i_modified_time`, `i_price_vat`, `i_price`, `i_category`, `i_group`) VALUES
(1, 'IPHONE20', 'made in china', 'IP002', 1, 'LGS0002', 'LGS0002', '2022-01-19 03:41:30', '2022-01-19 03:41:30', 7900, 7383.18, 'A002', NULL),
(2, 'IPHONE11', 'made in china', 'IP002', 1, 'LGS0002', 'LGS0002', '2022-01-19 03:41:40', '2022-01-19 03:41:40', 18900, 17663.6, 'A001', NULL),
(3, 'OPPO V99', 'made in china', 'IP002', 1, 'LGS0002', 'LGS0002', '2022-01-19 03:41:54', '2022-01-19 03:41:54', 39000, 36448.6, 'A003', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lsp_order`
--

CREATE TABLE `lsp_order` (
  `o_id` int(11) NOT NULL,
  `o_customer_name` varchar(100) DEFAULT NULL,
  `o_customer_lastname` varchar(100) DEFAULT NULL,
  `o_created_price` varchar(100) DEFAULT NULL,
  `o_customer_tel` varchar(100) DEFAULT NULL,
  `o_status` int(11) DEFAULT '1',
  `o_created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `o_modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `o_created_by` varchar(100) DEFAULT NULL,
  `o_modified_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lsp_order`
--

INSERT INTO `lsp_order` (`o_id`, `o_customer_name`, `o_customer_lastname`, `o_created_price`, `o_customer_tel`, `o_status`, `o_created_time`, `o_modified_time`, `o_created_by`, `o_modified_by`) VALUES
(1, 'sss', 'sss', '7900', '0837141140', 1, '2022-01-19 03:42:01', '2022-01-19 03:48:59', 'LGS0002', 'LGS0002');

-- --------------------------------------------------------

--
-- Table structure for table `lsp_order_detail`
--

CREATE TABLE `lsp_order_detail` (
  `od_id` int(11) NOT NULL,
  `od_status` int(11) DEFAULT '1',
  `od_created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `od_modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `items_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `od_created_by` varchar(100) DEFAULT NULL,
  `od_modified_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lsp_order_detail`
--

INSERT INTO `lsp_order_detail` (`od_id`, `od_status`, `od_created_time`, `od_modified_time`, `items_id`, `order_id`, `od_created_by`, `od_modified_by`) VALUES
(1, 0, '2022-01-19 03:42:01', '2022-01-19 03:48:59', 1, 1, 'LGS0002', 'LGS0002'),
(2, 0, '2022-01-19 03:42:01', '2022-01-19 03:48:59', 2, 1, 'LGS0002', 'LGS0002'),
(3, 0, '2022-01-19 03:42:01', '2022-01-19 03:48:59', 3, 1, 'LGS0002', 'LGS0002'),
(4, 0, '2022-01-19 03:49:28', '2022-01-19 03:50:02', 1, 1, 'LGS0002', 'LGS0002'),
(5, 1, '2022-01-19 03:50:02', '2022-01-19 03:50:02', 1, 1, 'LGS0002', 'LGS0002');

-- --------------------------------------------------------

--
-- Table structure for table `lsp_user`
--

CREATE TABLE `lsp_user` (
  `u_id` int(11) NOT NULL,
  `u_username` varchar(100) DEFAULT NULL,
  `u_firstname` varchar(100) DEFAULT NULL,
  `u_lastname` varchar(100) DEFAULT NULL,
  `u_password` text,
  `u_usercode` varchar(100) DEFAULT NULL,
  `u_status` int(1) DEFAULT '1',
  `u_created_by` varchar(100) DEFAULT NULL,
  `u_modified_by` varchar(100) DEFAULT NULL,
  `u_created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `u_modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_phonenumber` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lsp_user`
--

INSERT INTO `lsp_user` (`u_id`, `u_username`, `u_firstname`, `u_lastname`, `u_password`, `u_usercode`, `u_status`, `u_created_by`, `u_modified_by`, `u_created_time`, `u_modified_time`, `u_phonenumber`) VALUES
(1, 'korakod', 'korakod', 'sunatho', '$2b$10$AToAT8J5UYv./YhwTO0cE.tk9RxGOKoPE/FBa4jBO.3/7RQ2EwqDG', 'LGS0002', 1, 'LGS0001', 'LGS0001', '2022-01-18 14:55:02', '2022-01-18 14:55:02', NULL),
(2, 'aroundtheearth', 'korakod', 'sunatho', '$2b$10$na2BMFHbQrlyezRLBpyTqOo1wFoUPI5ud7FTLzEyRTvWpeoh0CL/e', 'LGS0003', 1, 'LGS0001', 'LGS0001', '2022-01-18 15:04:50', '2022-01-18 15:04:50', NULL),
(3, 'aroundtheearth', 'korakod', 'sunatho', '$2b$10$2YqHyhi7MkuaIXQzt4GwvOT6LZn83v/nS7CjSOdbnZKV08tIEwZ52', 'LGS0003', 1, 'LGS0001', 'LGS0001', '2022-01-18 15:05:37', '2022-01-18 15:05:37', NULL),
(4, 'aroundtheearth', 'korakod', 'sunatho', '$2b$10$t3DA12W168EXrUPZCb2LFO9tSre3HWcOyrhoLIiUV3VGxeD.QZn6S', 'LGS0004', 1, 'LGS0002', 'LGS0002', '2022-01-18 15:07:02', '2022-01-18 15:07:02', NULL),
(5, 'ssss', 'sss', 'ssss', '$2b$10$v/kIJfFeVMjBXtyzSKxd3OT4i1ws2L1OQhx/8b44X0ozYBgBjzx6u', 'LGS0005', 0, 'LGS0003', 'LGS0005', '2022-01-18 15:07:41', '2022-01-18 15:12:40', NULL),
(6, 'puriinzzz', 'puriinzzz', 'puriinzzz', '$2b$10$yXLLbsrFtp4euqYBo6.pf.e1.OdF7SDQXB4fgPE8ooDjIZSgXWVji', 'LGS0005', 1, 'LGS0002', 'LGS0002', '2022-01-19 04:03:39', '2022-01-19 04:03:39', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lsp_category`
--
ALTER TABLE `lsp_category`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `lsp_items`
--
ALTER TABLE `lsp_items`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `lsp_order`
--
ALTER TABLE `lsp_order`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `lsp_order_detail`
--
ALTER TABLE `lsp_order_detail`
  ADD PRIMARY KEY (`od_id`);

--
-- Indexes for table `lsp_user`
--
ALTER TABLE `lsp_user`
  ADD PRIMARY KEY (`u_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lsp_category`
--
ALTER TABLE `lsp_category`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lsp_items`
--
ALTER TABLE `lsp_items`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lsp_order`
--
ALTER TABLE `lsp_order`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lsp_order_detail`
--
ALTER TABLE `lsp_order_detail`
  MODIFY `od_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `lsp_user`
--
ALTER TABLE `lsp_user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
