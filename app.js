const express = require("express")
const app = express()
const bodyParser = require("body-parser")
global.moment = require('moment')
const port = 3456
const morgan = require("morgan")
require("dotenv").config()
const jwt = require("express-jwt")
const compression = require('compression')
const cors = require("cors")
const fs = require("fs")
app.use(compression())
app.use(
    morgan(
        ":method :url [:date[clf]] :status :res[content-length] - :response-time ms"
    )
)
global.router = express.Router();
let servers = app.listen(port, function (err) {
    if (err) {
        console.log("somthing went wrong", err)
    } else {
        console.log("Server is listening on port " + port)
    }
})
global.privateKeyPath = __dirname + "/middleware/PrivateKey.key"
const privateKey = fs.readFileSync(privateKeyPath, "utf8");
app.use(cors());
app.use("/public", express.static("public"));
app.use(
    bodyParser.urlencoded({
        limit: "20mb",
        extended: true
    })
)
app.use(bodyParser.json({
    limit: "20mb"
}))
app.use(
    jwt({
        secret: privateKey,
        algorithms: ["HS256"],
        credentialsRequired: true,
        getToken: function fromHeaderOrQuerystring(req) {
            if (
                req.headers.authorization &&
                req.headers.authorization.split(" ")[0] === "Bearer"
            ) {
                return req.headers.authorization.split(" ")[1];
            } else if (req.query && req.query.token) {
                return req.query.token;
            }
            return null;
        }
    }).unless({
        path: ["/Login/login"]
    })
)

app.get("/", function (req, res, next) {
    res.send(`listening port ${port}`)
})
app.use(require('./router'))