const db = require("../../../knex/knex")
const endcode = require('../../../function/endcode')
const getUser = async () => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_user').select(
                'u_id',
                'u_username',
                'u_firstname',
                'u_lastname',
                'u_phonenumber',
                'u_usercode'
            )
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'success', data: error })
        }
    })
}

const searchUser = async (data) => {
    try {
        let response = await db('lsp_user').select(
            'u_id',
            'u_username',
            'u_firstname',
            'u_lastname',
            'u_phonenumber',
            'u_usercode'
        ).where('u_username', 'like', "%" + data + "%").andWhere('u_status', 1).orWhere('u_firstname', 'like', "%" + data + "%").andWhere('u_status', 1)
            .orWhere('u_lastname', 'like', "%" + data + "%").andWhere('u_status', 1).orWhere('u_phonenumber', 'like', "%" + data + "%").andWhere('u_status', 1)
        return ({ status: 'success', data: response })
    } catch (error) {
        console.log(error)
        return ({ status: 'success', data: error })
    }
}
const addUser = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            data.u_created_by = user
            data.u_modified_by = user
            data.u_password = await endcode.hashData(data.u_password)
            let res = await db('lsp_user').insert(data)
            resolve({ status: 'success', data: res })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const editUser = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            data.u_modified_by = user
            data.u_password = await endcode.hashData(data.u_password)
            let response = await db('lsp_user').update(data).where('u_id', data.u_id)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const deleteUser = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_user').update({
                u_status: 0,
                u_modified_by: user
            }).where('u_id', data.u_id)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}

module.exports = { getUser, addUser, editUser, deleteUser, searchUser }