const express = require("express")
const router = express.Router();

router.use(require('../user/controllers/user'))

module.exports = router