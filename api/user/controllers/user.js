const express = require("express");
const router = express.Router();
const func = require('../function/user')
router
    .route("/getUser")
    .get(async (req, res) => {
        try {
            let response = await func.getUser(req.body)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/addUser")
    .post(async (req, res) => {
        try {
            let response = await func.addUser(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/editUser")
    .patch(async (req, res) => {
        try {
            let response = await func.editUser(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/deleteUser")
    .delete(async (req, res) => {
        try {
            let response = await func.deleteUser(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })



router
    .route("/searchUser")
    .post(async (req, res) => {
        try {
            let response = await func.searchUser(req.body.keyword)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })

module.exports = router 