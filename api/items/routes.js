const express = require("express")
const router = express.Router();

router.use(require('../items/controllers/items'))

module.exports = router