const express = require("express");
const router = express.Router();
const func = require('../function/Items')
router
    .route("/getItems")
    .get(async (req, res) => {
        try {
            let response = await func.getAllItems()
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })

router
    .route("/getItemsByCategory")
    .get(async (req, res) => {
        try {
            let response = await func.getItemsByCategory(req.body.i_category)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/searchItems")
    .post(async (req, res) => {
        try {
            let response = await func.searchItems(req.body.keyword)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/addItems")
    .post(async (req, res) => {
        try {
            let response = await func.addItems(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/editItems")
    .patch(async (req, res) => {
        try {
            let response = await func.editItems(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/deleteItems")
    .delete(async (req, res) => {
        try {
            let response = await func.deleteItems(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })

module.exports = router 