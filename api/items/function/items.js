const db = require("../../../knex/knex")
const endcode = require('../../../function/endcode')


const getAllItems = async () => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_items').select(
                'i_id',
                'i_name',
                'i_description',
                'i_code',
                'i_status',
                'i_price_vat',
                'i_price',
                'i_category',
            ).andWhere('i_status', 1)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'success', data: error })
        }
    })
}
const addItems = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            data.i_price = await removeVat(data.i_price_vat)
            data.i_created_by = user
            data.i_modified_by = user
            let res = await db('lsp_items').insert(data)
            resolve({ status: 'success', data: res })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const editItems = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            data.i_price = await removeVat(data.i_price_vat)
            data.i_modified_by = user
            let response = await db('lsp_items').update(data).where('i_id', data.i_id)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const deleteItems = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_items').update({
                i_status: 0,
                i_modified_by: user
            }).where('i_id', data.i_id)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const removeVat = async (price) => {
    try {
        return (parseFloat(price) * 100) / 107
    } catch (error) {
        return (price)
    }
}
const getItemsByCategory = async (category) => {
    try {
        let response = await db('lsp_items').select(
            'i_id',
            'i_name',
            'i_description',
            'i_code',
            'i_status',
            'i_price_vat',
            'i_price',
            'i_category',
        ).where('i_category', category).andWhere('i_status', 1)
        return ({ status: 'success', data: response })
    } catch (error) {
        return ({ status: 'fail', data: error })
    }
}
const searchItems = async (data) => {
    try {
        let response = await db('lsp_items').select(
            'i_name',
            'i_description',
            'i_code',
            'i_status',
            'i_price_vat',
            'i_price',
            'i_category',
            'i_group'
        ).where('i_name', 'like', "%" + data + "%").andWhere('i_status', 1).orWhere('i_description', 'like', "%" + data + "%").andWhere('i_status', 1)
            .orWhere('i_code', 'like', "%" + data + "%").andWhere('i_status', 1)
        return ({ status: 'success', data: response })
    } catch (error) {
        console.log(error)
        return ({ status: 'success', data: error })
    }
}
module.exports = { getAllItems, addItems, editItems, deleteItems, getItemsByCategory, searchItems }