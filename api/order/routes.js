const express = require("express")
const router = express.Router();

router.use(require('../order/controllers/order'))

module.exports = router