const express = require("express");
const router = express.Router();
const func = require('../function/order')
router
    .route("/getOrder")
    .get(async (req, res) => {
        try {
            let response = await func.getOrder()
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/getOrderByCustomer")
    .get(async (req, res) => {
        try {
            let response = await func.getOrderByCustomer(req.body.keyword)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })

router
    .route("/addOrder")
    .post(async (req, res) => {
        try {
            let response = await func.addOrder(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/editOrder")
    .patch(async (req, res) => {
        try {
            let response = await func.editOrder(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/deleteOrder")
    .delete(async (req, res) => {
        try {
            let response = await func.deleteOrder(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })

module.exports = router 