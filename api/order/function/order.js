const db = require("../../../knex/knex")
const getOrder = async () => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_order').select(
                'o_id',
                'o_customer_name',
                'o_customer_lastname',
                'o_created_price',
                'o_customer_tel',
                'o_created_by',
                'o_modified_by',
            ).where('o_status', 1)
            for (let round of response) {
                let sumPriceVat = 0
                let sumPrice = 0
                round.detail = await db.select(
                    'i.i_id',
                    'i.i_name',
                    'i.i_description',
                    'i.i_code',
                    'i.i_price_vat',
                    'i.i_price'
                ).from({ od: 'lsp_order_detail' }).leftJoin({ i: 'lsp_items' }, 'od.items_id', 'i.i_id')
                    .where('order_id', round.o_id).andWhere('i.i_status', 1).andWhere('od.od_status', 1)
                for (element of round.detail) {
                    sumPriceVat += parseFloat(element.i_price_vat)
                    sumPrice += parseFloat(element.i_price)
                }
                round.sumPriceVat = sumPriceVat
                round.sumPrice = sumPrice
            }
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'success', data: error })
        }
    })
}
const getOrderByCustomer = async (keyword) => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_order').select(
                'o_id',
                'o_customer_name',
                'o_customer_lastname',
                'o_created_price',
                'o_customer_tel',
                'o_created_by',
                'o_modified_by',
            ).where('o_customer_name', 'like', "%" + keyword + "%").andWhere('o_status', 1)
                .orWhere('o_customer_lastname', 'like', "%" + keyword + "%").andWhere('o_status', 1)
                .orWhere('o_customer_tel', 'like', "%" + keyword + "%").andWhere('o_status', 1)
            let sumPriceVat = 0
            let sumPrice = 0
            for (let round of response) {
                round.detail = await db.select(
                    'i.i_name',
                    'i.i_description',
                    'i.i_code',
                    'i.i_price_vat',
                    'i.i_price'
                ).from({ od: 'lsp_order_detail' }).leftJoin({ i: 'lsp_items' }, 'od.items_id', 'i.i_id')
                    .where('order_id', round.o_id)
                for (element of round.detail) {
                    sumPriceVat += parseFloat(element.i_price_vat)
                    sumPrice += parseFloat(element.i_price)
                }
            }
            resolve({ status: 'success', data: response, sumPriceVat: sumPriceVat, sumPrice: sumPrice })
        } catch (error) {
            console.log(error)
            resolve({ status: 'success', data: error })
        }
    })
}
const addOrder = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            let price = await calculatePrice(data.items)
            let orderid = await db('lsp_order').insert({
                o_customer_name: data.o_customer_name,
                o_customer_lastname: data.o_customer_lastname,
                o_customer_tel: data.o_customer_tel,
                o_created_price: price,
                o_created_by: user,
                o_modified_by: user
            })
            await addOrderDetail(data.items, user, orderid)
            resolve({ status: 'success', data: 'res' })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const addOrderDetail = async (items, user, orderid) => {
    return new Promise(async (resolve) => {
        try {
            for (let element of items) {
                await db('lsp_order_detail').insert({
                    od_created_by: user,
                    od_modified_by: user,
                    order_id: orderid,
                    items_id: element
                })
            }
            resolve({ status: 'success' })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail' })
        }
    })
}
const editOrder = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            data.o_modified_by = user
            let price = await calculatePrice(data.items)
            let response = await db('lsp_order').update({
                o_customer_name: data.o_customer_name,
                o_customer_lastname: data.o_customer_lastname,
                o_customer_tel: data.o_customer_tel,
                o_created_price: price,
                o_modified_by: user
            }).where('o_id', data.o_id)
            let a = await db('lsp_order_detail').update({
                od_status: 0
            }).where('order_id', data.o_id)
            console.log(a)
            await addOrderDetail(data.items, user, data.o_id)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const deleteOrder = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_order').update({
                o_status: 0,
                o_modified_by: user
            }).where('o_id', data.o_id)

            await db('lsp_order_detail').update({
                od_status: 0,
                od_modified_by: user
            }).where('order_id', data.o_id)

            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const calculatePrice = async (items) => {
    try {

        let price = 0
        for (let element of items) {
            let item = await db('lsp_items').select('i_price_vat').where('i_id', element)
            price += parseFloat(item[0].i_price_vat)
        }
        return price
    } catch (error) {
        console.log(error)
    }
}
module.exports = { addOrder, getOrder, deleteOrder, editOrder, getOrderByCustomer }