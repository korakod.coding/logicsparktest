const db = require("../../../knex/knex")
const endcode = require('../../../function/endcode')
const login = async (data) => {
    try {
        let user = await db('lsp_user').select('u_id', 'u_username', 'u_firstname', 'u_lastname', 'u_password', 'u_usercode')
            .where('u_username', data.u_username)
            .andWhere('u_status', 1)
        if (user.length) {
            let userpwd = user[0].u_password;
            let user_profile = {
                u_username: user[0].u_username,
                u_firstname: user[0].u_firstname,
                u_lastname: user[0].u_lastname,
                u_id: user[0].u_id,
                u_usercode: user[0].u_usercode
            }
            console.log(await endcode.compareData(data.u_password, userpwd))
            if (await endcode.compareData(data.u_password, userpwd)) {
                let response = await endcode.jwtEncode(user_profile)
                return ({ msg: "Authenticated", token: response });
            } else {
                return ({ msg: "UnAuthenticated" })
            }
        }
    } catch (error) {
        console.log(error)
    }
}

module.exports = { login }
