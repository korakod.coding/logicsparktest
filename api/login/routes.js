const express = require("express")
const router = express.Router();

router.use(require('../login/controllers/login'))

module.exports = router