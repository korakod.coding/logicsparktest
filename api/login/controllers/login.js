const express = require("express");
const router = express.Router();
const func = require('../function/login')

router
    .route("/login")
    .post(async (req, res) => {
        try {
            let response = await func.login(req.body)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })


module.exports = router 