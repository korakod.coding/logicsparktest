const express = require("express");
const router = express.Router();
const func = require('../function/category')
router
    .route("/getCategory")
    .get(async (req, res) => {
        try {
            let response = await func.getCategory()
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/addCategory")
    .post(async (req, res) => {
        try {
            let response = await func.addCategory(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/editCategory")
    .patch(async (req, res) => {
        try {
            let response = await func.editCategory(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
router
    .route("/deleteCategory")
    .delete(async (req, res) => {
        try {
            let response = await func.deleteCategory(req.body, req.user.u_usercode)
            res.status(200).json(response)
        } catch (error) {
            console.log(error)
            res.status(500).json({ 'error': error })
        }
    })
module.exports = router 