const express = require("express")
const router = express.Router();

router.use(require('../category/controllers/category'))

module.exports = router