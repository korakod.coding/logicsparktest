const db = require("../../../knex/knex")
const endcode = require('../../../function/endcode')


const getCategory = async () => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_category').select(
                'c_id',
                'c_code',
                'c_name',
                'c_description'
            ).andWhere('c_status', 1)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'success', data: error })
        }
    })
}
const addCategory = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            data.c_created_by = user
            data.c_modified_by = user
            let res = await db('lsp_category').insert(data)
            resolve({ status: 'success', data: res })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const editCategory = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            data.c_modified_by = user
            let response = await db('lsp_category').update(data).where('c_id', data.c_id)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}
const deleteCategory = async (data, user) => {
    return new Promise(async (resolve) => {
        try {
            let response = await db('lsp_category').update({
                c_status: 0,
                c_modified_by: user
            }).where('c_id', data.c_id)
            resolve({ status: 'success', data: response })
        } catch (error) {
            console.log(error)
            resolve({ status: 'fail', data: error })
        }
    })
}


module.exports = { getCategory, addCategory, deleteCategory, editCategory }